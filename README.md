BayONet 1.0 : Bayesian Optimization for biochemical Networks

BayONet is a prototype that performs parameter estimation for biochemical networks, it relies on the use of Bayesian networks.
It runs using GNU Octave. 
To run an example, run one of the following:
- To run the toy example, run gen_toy.m in the toy_example directory 
- To run the enzyme catalyzed reaction example, run gen_catalyzed_mats.m in the enzyme_catalyzed directory with 4 arguments to generate CPT tables and run catalyzed_optim.m with the same arguments to perform parameter estimation using the computed CPTs. The arguments are the following:
    - arg1 : integer, number of discrete states for state variables (e.g. 5)
    - arg2 : integer, number of discrete states for parameters (e.g. 5)
    - arg3 : integer, number of simulations used to compute the CPTs (e.g. 100000)
    - arg4 : character string, name of the numerical simulation scheme (e.g. euler or rk4)

For example run "octave gen_catalyzed_mats.m 100000 5 5 rk4" then run "octave catalyzed_optim.m 100000 5 5 rk4"
- To run the EGF-NGF model, run gen_egf_mat.m in the egf_ngf directory to compute CPTs, and run egf_mat_optim.m to perform parameter estimation


Installation:
- Requires GNU Octave  (tested on version 7.1)
- /!\ To clone this project use the --recurse-submodules option to load the BNT toolbox it depends on /!\ 