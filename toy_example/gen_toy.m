%EGF sim

warning("off");

var = [
    1.5 0 10 1; %4
    8.5 0 10 1
    ];
varname = {"x", "y"};

par = [
    0.19 0 1 0.5; %10
    ];
parname = {"k"};

function xdot = f (x, t, k)
    xdot(1) = k(1) * x(1);
    xdot(2) = -0.9 * k(1) * x(1);
endfunction

dependencypar = [[1]; %x1 depends on k1 and k2
            [1]
            ];
dependencyvar = [[1 0];
            [1 1]%x1 depends on x1, x2 and x3
            ];

t = linspace(0, 10, 11);

% x = lsode(@(x,t) f(x,t,0.19),var(:,1),t);
% plot(t,x);
% legend ({"x", "y"}, "location", "east");
% keyboard

addpath("../engine");

%Compute sim %%%%%%
bncpd = gen_bncpd_nomap(var, par, dependencyvar, dependencypar, t, 201, true);
save("bayesian_cpd.mat", "bncpd");

%build bnet %%%%%
addpath("../bnt");
addpath(genpathKPM("../bnt"));
%load("bayesian_cpd.mat");
bn = mk_static_bayesian(var, par, dependencyvar, dependencypar, varname, parname, bncpd);
%keyboard

sim_bayesian_traj(bn, var, par, t);
keyboard;

dbn = mk_dynamic_bayesian(var, par, dependencyvar, dependencypar, varname, parname, bncpd);

### INFERENCE

# for static Bnet:  engine = jtree_inf_engine(bnet);
#engine = smoother_engine(hmm_2TBN_inf_engine(dbn));
engine = jtree_inf_engine(bn);
#engine = smoother_engine(jtree_2TBN_inf_engine(dbn));
#ev = sample_dbn(dbn,2);
evidence = cell(1, 11);
[engine, loglik] = enter_evidence(engine, evidence);
marg = marginal_nodes(engine, 8);

#evidence = cell(7,2);

# APPRENTISSAGE DE PARAMETRES
N = 11;
nsamples = 500;
samples = cell(N, nsamples);

for i = 1:nsamples
    samples(:, i) = sample_bnet(bn);
end

% Make a tabula rasa
%bnet2 = mk_bnet(dag_egf,[mk_nodesize(var) mk_nodesize(par) mk_nodesize(var)], 'names', {'x1','x2','x3','x4','k1','k2','k3','x1p','x2p','x3p','x4p'});
bnet2 = mk_static_bayesian(var, par, dependencyvar, dependencypar, varname, parname, []);
seed = 0;
rand('state', seed);
bnet2.CPD{1} = tabular_CPD(bn, 1);
bnet2.CPD{2} = tabular_CPD(bn, 2);
bnet2.CPD{3} = tabular_CPD(bn, 3);
bnet2.CPD{4} = tabular_CPD(bn, 4);
bnet2.CPD{5} = tabular_CPD(bn, 5);
bnet2.CPD{6} = tabular_CPD(bn, 6);
bnet2.CPD{7} = tabular_CPD(bn, 7);
bnet2.CPD{8} = tabular_CPD(bn, 8);
bnet2.CPD{9} = tabular_CPD(bn, 9);
bnet2.CPD{10} = tabular_CPD(bn, 10);
bnet2.CPD{11} = tabular_CPD(bn, 11);

bnet3 = learn_params(bnet2, samples);

CPT3 = cell(1, N);

for i = 1:N
    s = struct(bnet3.CPD{i}); % violate object privacy
    CPT3{i} = s.CPT;
end

dispcpt(CPT3{4})

keyboard
# APPRENTISSAGE PARAMETRES AVEC DONNEES INCOMPLETES

samples2 = samples;
hide = rand(N, nsamples) > 0.5;
[I, J] = find(hide);

for k = 1:length(I)
    samples2{I(k), J(k)} = [];
end

engine2 = jtree_inf_engine(bnet2);
max_iter = 2;
[bnet4, LLtrace] = learn_params_em(engine2, samples2, max_iter);

CPT4 = cell(1, N);

for i = 1:N
    s = struct(bnet4.CPD{i}); % violate object privacy
    CPT4{i} = s.CPT;
end

dispcpt(CPT4{4})

keyboard
