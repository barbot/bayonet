function [err_best,par_best] = inference_search(var, par, t, bn, B_size, expdata, timedata, vardim, pardim, ntime, B_size_par, M, x_ev, oparams, uparams)



    % end

    tab_res = [];
    [nsample, vardim] = size(expdata);
    pardim = length(par(:, 1));
    % t = linspace(0, 101, 10);

    % x = lsode(@(x, t) f(x, t, par(:, 1)), var(:, 1), t);
    % plot(t, x);
    % legend ({"x1", "x2", "x3", "x4"}, "location", "east");

    % expdata = x;
    samples = cell(1, vardim + pardim + vardim);

    par_res = zeros(pardim, nsample, B_size);
    par_moy = zeros(pardim, B_size);
    par_best = ones(1, pardim);

    for i = 1:nsample - 1
        # SAMPLES FOR BN
        xd = discretize(expdata(i, 1:vardim), var) +1; % /!\ discretize numerote à partir de zero /!\\
        xdtp1 = discretize(expdata(i + 1, 1:vardim), var) +1;

        for k = 1:vardim
            samples(1, k) = xd(k);
            samples(1, vardim + pardim + k) = xdtp1(k);
        end

        % evetuellement parametre connus :  samples(4 + known_param, cpt) = nk(known_param);

        engine = jtree_inf_engine(bn);
        [engine, loglik] = enter_evidence(engine, samples);

        for j = uparams
            #  for b = 1:B_size_par
            #    evidence{vardim+b} = b
            marg(i) = marginal_nodes(engine, vardim + j);

            for k = 1:B_size
                par_res(j, i, k) = marg(i).T(k);
            end

            % keyboard

            % cpt++;
        endfor

        % err_res = err_current;
        % res = par_best;
        % keyboard
    end

    for i = uparams

        for k = 1:B_size

            for j = 1:nsample

                par_moy(i, k) = par_moy(i, k) + par_res(i, j, k);

            end

            par_moy(i, k) = 1 / nsample * par_moy(i, k);

        end

        [maxp, idmax] = max(par_moy(i, :));
        par_best(i) = idmax;
        % [max, idmax] = max()
    end


    res_best = simulate_bn(vardim, pardim, ntime, B_size, M, bn, x_ev, par_best, oparams, uparams);
    err_best = eval(res_best, expdata, timedata);


end
