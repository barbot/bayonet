function cp = reshape_transpose (cpd, vdef, i)
  low = vdef(i,2);
  up = vdef(i,3);
  step = vdef(i,4);
  nbucket = (up-low)/step;
  ncpd = length(cpd);
  cp = reshape(cpd,[nbucket,ncpd/nbucket])'; #'
endfunction