function [out, maxx] = discretize (x, var)# calcule dans quel intervalle est var
    %to vectorize
    x = x';
    [m n] = size(x);

    for i = 1:m
        low = var(i, 2);
        up = var(i, 3);
        step = var(i, 4);
        maxx(i) = int64((up - low) / step);

        out(i) = max(0, min(floor ((x(i) - low) / step), maxx(i) - 1));
        
        % #floor(((double(low) + double(it) * step) / step));
        % keyboard
    endfor

    #keyboard
    #numerotation commence a zero
endfunction
