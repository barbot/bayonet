function [err_res, par_res] = naive_search(x_ev, vardim, pardim, ntime, B_size, M, bn, expdata, timedata)

    params_set = dec2base(0:B_size^pardim - 1, B_size);
    [len, dim] = size(params_set)
    param_vec = zeros(1, pardim);
    tab_res = [];

    for i = 1:len

        for k = 1:pardim
            param_vec(k) = base2dec(params_set(i, k), B_size) + 1;
        endfor

        param_vec

        res = simulate_bn(vardim, pardim, ntime, B_size, M, bn, x_ev, param_vec);
        tab_res = [tab_res; [param_vec, eval(res, expdata, timedata)]];

    endfor

    [err_res, idx] = min(tab_res(:, end));
    par_res = tab_res(idx, 1:pardim);

    % keyboard
end
