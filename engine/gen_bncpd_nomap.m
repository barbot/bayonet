function [bncpd] = gen_bncpd_nomap(var, par, dependencyvar, dependencypar, t, nb_samples, do_randomize)
    global timeode;
    global timemapid;
    [var_samples, par_samples] = sample_halton(nb_samples, var, par);
    sample_it_var = 1;
    sample_it_par = 1;
    #keyboard
    [nvar, _] = size(var);
    [npar, _] = size(par);
    [_ max_id_var] = map_to_id(zeros(1, nvar), var, dependencyvar); %x1 ,x2, ....
    [_ max_id_par] = map_to_id(zeros(1, npar), par, dependencypar); %x1 ,x2, ....
    [_ max_id_x] = discretize(zeros(1, nvar), var); %x
    max_key = max_id_var .* max_id_x .* max_id_par;

    for j = 1:nvar
        map{1, j} = sparse(max_key(j), 1); %containers.Map({int64(0)},{0});
        map_sum{1, j} = sparse(max_key(j), 1); % containers.Map({int64(0)},{1});
    endfor

    %[map, map_sum] = gen_map(1,n);
    for i = 1:nb_samples
        disp(i)
        [id, x, map, map_sum, sample_it_var, sample_it_par] = sample_disc(t, map, map_sum, par, par_samples, sample_it_par, dependencypar, var, var_samples, sample_it_var, dependencyvar, do_randomize);
        %disp(timeode)
        %disp(timemapid)
        #keyboard
    endfor

    for i = 1:nvar
        bncpd{i} = gen_tab2(i, var, par, map, map_sum, dependencyvar, dependencypar);
    endfor

endfunction

function [id, maxid] = map_to_id(x, var, dependency)# retourne l'id correspondant au x courant
    maxid = int64(1);
    %[_,n] = size(x); %m=1, n taille du vecteur variable /parameter
    [nvar, n] = size(dependency); %number of variable
    id = int64 (zeros(1, nvar));
    maxid = int64 (ones(1, nvar));

    for i = 1:n
        #keyboard
        low = var(i, 2);
        up = var(i, 3);
        step = var(i, 4);

        if (step != 0)
            maxx = int64((up - low) / step);
        else
            maxx = 1;
            step = 1;
        end

        it = max(0, min(int64 (floor ((x(i) - low) / step)), maxx - 1));

        depi = dependency(:, i);

        id += it * maxid .* depi';
        maxid = maxid .* (1 + depi' * (maxx - 1));

        % keyboard
    endfor

    #keyboard
endfunction

% function incr_map(map, id)

%     if isKey(map, id)
%         map(id) = map(id) + 1;
%     else
%         map(id) = 1;
%     endif

% endfunction

function [out, x, map, map_sum, sample_it_var, sample_it_par] = sample_disc(t, map, map_sum, par, par_samples, sample_it_par, dependencypar, var, var_samples, sample_it_var, dependencyvar, do_randomize)# simule une tajectoire, et ajoute le resultat dans map/map_sum pour compter les transitions

    if nargin < 8
        do_randomize = true;
    endif

    global timeode;
    global timemapid;
    [n, _] = size(var); %number of variable

    if do_randomize
        #k0 = sample_init(par); %par(:,1);
        k0 = par_samples(sample_it_par, :);
        x0 = var_samples(sample_it_var, :);
        sample_it_par++;
        sample_it_var++;
    else
        k0 = par(:, 1);
        x0 = var(:, 1);
    endif

    [id_par, max_id_par] = map_to_id(k0, par, dependencypar); %k1, k2, ...
    fk = @(x, t) f(x, t, k0);
    id = tic();
    x = lsode(fk, x0, t);
    #plot(t,x);
    #keyboard
    timeode += toc (id);

    for ti = 1:length(t) - 1
        [id_var max_id_var] = map_to_id(x(ti, :), var, dependencyvar); %x1 ,x2, ....
        [id_x max_id_x] = discretize(x(ti + 1, :), var); %x
        %discretize(x(ti,:),var)

        % keyboard
        # AVANT: out = (id_var + max_id_var.* id_x);
        # APRES:
        out_total = id_var + max_id_var .* id_par;

        out = id_x + max_id_x .* out_total; % id_var;
        id2 = tic();
        % keyboard

        for i = 1:n
            map{1, i}(int32(out(i) + 1)) += 1;
            map_sum{1, i}(int32(out_total(i) + 1)) += 1;
            %incr_map(map{1,i}, int64(out(i)));
            %incr_map(map_sum{1,i}, int64(out_total(i)));
        endfor

        timemapid += toc(id2);
        #keyboard

    endfor

endfunction

% function [map, map_sum] = gen_map(tlength, varlength)

%     for i = 1:1

%         for j = 1:varlength
%             map{i, j} = zeros(40000, 1); %containers.Map({int64(0)},{0});
%             map_sum{i, j} = zeros(40000, 1); % containers.Map({int64(0)},{1});
%         endfor

%     endfor

% endfunction

function [tab] = gen_tab2(v, var, par, map, map_sum, dependencyvar, dependencypar)# calcule les tables de proba
    [n, _] = size(var);
    [npar, _] = size(par);

    %mks = mk_nodesize(var);

    #tab = zeros(,n);
    m = map{1, v};
    ms = map_sum{1, v};
    [_ max_id_var] = map_to_id(zeros(1, n), var, dependencyvar); %x1 ,x2, ....
    [_ max_id_par] = map_to_id(zeros(1, npar), par, dependencypar); %x1 ,x2, ....
    [_ max_id_x] = discretize(zeros(1, n), var); %x
    %max_key = m.keys{m.Count};
    max_key = max_id_var(v) * max_id_x(v) * max_id_par(v);

    #tab = sparse(max_key,1);
    tab = zeros(max_key, 1);
    low = var(v, 2);
    up = var(v, 3);
    step = var(v, 4);
    max = int64((up - low) / step);
    % for i=1:length(m)
    %         %if (ms(idivide(i-1,max)+1)!= 0)
    %         if ( m(i) != 0)
    %           tab(i) = m(i)/ms(idivide(i-1,max)+1);
    %         else if (ms(idivide(i-1,max)+1)== 0)
    %           %disp(i);
    %           tab(i) = 1.0/max;
    %         endif
    %         endif
    %
    keys = find(m);
    % keyboard

    for i = 1:length(keys)

        %if (ms(idivide(i-1,max)+1)!= 0)
        % keyboard
        tab(keys(i)) = m(keys(i)) / ms(idivide(keys(i) - 1, max) + 1);
        % if ( m(i) != 0)
        %   tab(i) = m(i)/ms(idivide(i-1,max)+1);
        % else if (ms(idivide(i-1,max)+1)== 0)
        %   %disp(i);
        %   tab(i) = 0;#1.0/double(max);
        % endif
        % endif
    endfor

    % keyboard
endfunction
