function M = lumping(M, i, j)
    M(i, :) = M(i, :) + M(j, :);
    M(j, :) = [];
endfunction
