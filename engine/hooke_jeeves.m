function [err_res, par_res] = hooke_jeeves(par_init, x_ev, vardim, pardim, ntime, B_size_par, M, bn, expdata, timedata, oparams, uparams)
    par_current = par_init;
    par_best = par_current;
    vec_depl = zeros(1, length(uparams));
    vec_mem = zeros(1, length(uparams));
    it = 0;

    par_current = par_best;
    par_old = par_current;
% keyboard
    % keyboard
    res_current = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current, oparams, uparams);
    err_current = eval(res_current, expdata, timedata);
    err_current_mem = err_current;

    while (true)

        for i = 1:length(uparams)

            vec_depl = zeros(1, pardim);
            vec_depl(uparams(i)) = 1;
            err_current = err_current_mem;
            % keyboard

            if (((par_current(uparams(i)) + vec_depl(uparams(i))) >= 1) && ((par_current(uparams(i)) + vec_depl(uparams(i))) <= B_size_par))
                par_current + vec_depl;
                res = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current + vec_depl);
                err = eval(res, expdata, timedata);

                if (err < err_current)
                    par_best = par_best + vec_depl;
                    % err_current = err
                    % vec_mem(i) = vec_mem(i) + vec_depl(i);

                else
                    vec_depl = zeros(1, pardim);
                    vec_depl(uparams(i)) = -1;

                    if (((par_current(uparams(i)) + vec_depl(uparams(i))) >= 1) && ((par_current(uparams(i)) + vec_depl(uparams(i))) <= B_size_par))
                        par_current + vec_depl;
                        res = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current + vec_depl);
                        err = eval(res, expdata, timedata);

                        if (err < err_current)
                            % vec_mem(i) = vec_mem(i) + vec_depl(i);
                            par_best = par_best + vec_depl;
                            % err_current = err

                        end

                    end

                end

            else
                vec_depl = zeros(1, pardim);
                vec_depl(uparams(i)) = -1;

                if (((par_current(uparams(i)) + vec_depl(uparams(i))) >= 1) && ((par_current(uparams(i)) + vec_depl(uparams(i))) <= B_size_par))
                    par_current + vec_depl;
                    res = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current + vec_depl);
                    err = eval(res, expdata, timedata);

                    if (err < err_current)
                        % vec_mem(i) = vec_mem(i) + vec_depl(i);
                        par_best = par_best + vec_depl;

                        % err_current = err

                    end

                end

            end

            % par_current = par_best;
            % res_current = simulate_bn(vardim, pardim, ntime, engine, B_size, M, bn, par_current);
            % err_current = eval(res_current, expdata, timedata);
            % err_current_mem = err_current;

        end

        it++

        if norm(par_old - par_best) == 0

            err_res = err_current;

            par_res = par_current;
            break

        else

            res_current = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_best);
            err_current = eval(res_current, expdata, timedata);

            if err_current < err_current_mem
                err_res = err_current;

                par_current = par_best;
                par_old = par_current;
                err_current_mem = err_current;

            else
                err_res = err_current_mem;

                par_res = par_current;

                break

            end

            %     parr_current = par_best;
            %     res_current = simulate_bn(vardim, pardim, ntime, engine, B_size, M, bn, par_current);
            %     err_current = eval(res_current, expdata, timedata);

        end

        % if

        %     res = simulate_bn(vardim, pardim, ntime, engine, B_size, M, bn, par_best);
        %     err = eval(res, expdata, timedata);

        %     if err < err_current
        %         par_current = par_best;
        %     end

        % if vec_mem == zeros(1, pardim);
        %     par_current
        %     break

        % end
        % end

        err_res = err_current;
        par_res = par_best;
        % keyboard
    end
