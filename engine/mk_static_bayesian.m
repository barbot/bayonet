
function bn = mk_static_bayesian(var,par,dependencyvar,dependencypar, varname, parname, cpd)
  nvar = size(dependencyvar)(1,1);
 %ncvar = nlvar
 % ncvar = size(dependencyvar)(1,2);
 % nlpar = ncvar
 %  nlpar = size(dependencypar)(1,1);
  npar = size(dependencypar)(1,2);

%pas sur que toutees les dimension soit les bonnes.
  dag_egf = [zeros(nvar,npar+nvar),dependencyvar';
            zeros(npar,npar+nvar),dependencypar';
         zeros(nvar,npar+nvar+nvar)];

  varsize = mk_nodesize(var);
  parsize = mk_nodesize(par);
  bn = mk_bnet(dag_egf,[varsize parsize varsize], 'names', [varname,parname,strcat(varname,"p")]);

  if size(cpd)!=0
    for i=1:nvar
      bn.CPD{i} = tabular_CPD(bn, i, 1/varsize(i) * ones(varsize(i),1));
    endfor
    for i=1+nvar:nvar+npar
      bn.CPD{i} = tabular_CPD(bn, i, 1/parsize(i-nvar) * ones(parsize(i-nvar),1));
    endfor
    for i=1+nvar+npar:nvar+npar+length(cpd)
      #bn.CPD{i} = tabular_CPD(bn, i, full(cpd{i-nvar-npar}));
      #keyboard
      bn.CPD{i} = tabular_CPD(bn, i, reshape_transpose(full(cpd{i-nvar-npar}),var,i-nvar-npar));

    endfor
  else
    for i = 1:nvar+npar+nvar
      bn.CPD{i} = tabular_CPD(bn, i);
    end
  end
endfunction



function node_sizes = mk_nodesize(var) # calcule le nombre d'intervalles par variables/parametre
  [m n] = size(var);
  for i=1:m
    low = var(i,2);
    up = var(i,3);
    step = var(i,4);
    node_sizes(i)=max(1,(up-low)/step);
  endfor
endfunction

