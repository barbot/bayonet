function out = sample_init(var) # calcule une valeur initiale au hasard
      [m,n] = size(var);
      for i=1:m
        low = var(i,2);
        up = var(i,3);
        out(1,i) = low + (up-low)*rand();
      endfor
endfunction