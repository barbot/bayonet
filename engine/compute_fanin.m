function [fanin] = compute_fanin(dependencyvar,dependencypar,best_pairs,dim_red)
    

    for i = 1:dim_red

        for j = 1:length(dependencyvar)

            if ((j != best_pairs(i, 2)) && (dependencyvar(best_pairs(i, 2), j) == 1))
                dependencyvar(best_pairs(i, 1), j) = 1;
            endif

            if (dependencyvar(j, best_pairs(i, 2)) == 1)
                dependencyvar(j, best_pairs(i, 1)) = 1;
            endif

            % if ((j != best_pairs(i, 2)) && (dependencyvar(best_pairs(i, 2), j) == 1))
            %     dependencyvar(best_pairs(i, 1), j) = 1;
            % endif

        endfor

        dependencyvar(best_pairs(i, 2), :) = [];
        dependencyvar(:, best_pairs(i, 2)) = [];

        for j = 1:length(dependencypar(1, :))

            if (dependencypar(best_pairs(i, 2), j) == 1)
                dependencypar(best_pairs(i, 1), j) = 1;
            endif

        endfor

        dependencypar(best_pairs(i, 2), :) = [];

        

        % dependencyvar(best_pairs(i, 1), )
    endfor

% keyboard

    fanin = max(sum(dependencyvar,2)+sum(dependencypar,2));

    % keyboard
endfunction