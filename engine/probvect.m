function v = probvect(vdef, x)
    [npar, _] = size(vdef);

    for i = 1:npar
        low = vdef(i, 2);
        up = vdef(i, 3);
        step = vdef(i, 4);

        for j = 1:int32((up - low) / step)
            v(i, j) = (x(i) == j);
        endfor

    endfor

endfunction
