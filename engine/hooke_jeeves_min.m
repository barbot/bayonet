function [err_res, par_res] = hooke_jeeves_min(par_init, x_ev, vardim, pardim, ntime, B_size_par, M, bn, expdata, timedata, oparams, uparams)
    par_current = par_init;
    vec_depl = zeros(1, pardim);
    vec_mem = zeros(1, pardim);
    it = 0;

    % par_current = par_best;
    % par_old = par_current;
    res_current = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current);
    err_current = eval(res_current, expdata, timedata);
    % err_current_mem = err_current;

    while (true)
        par_best = par_current;
        par_old = par_current;

        for i = 1:length(uparams)

            vec_depl = zeros(1, pardim);
            vec_depl(uparams(i)) = 1;
            % err_current = err_current_mem;

            if (((par_current(uparams(i)) + vec_depl(uparams(i))) >= 1) && ((par_current(uparams(i)) + vec_depl(uparams(i))) <= B_size_par))
                par_current + vec_depl;
                res = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current + vec_depl);
                err = eval(res, expdata, timedata);

                if (err < err_current)
                    err_current = err;
                    err_res = err_current;
                    par_best = par_current + vec_depl;
                    % vec_mem(i) = vec_mem(i) + vec_depl(i);

                end

            end

            vec_depl = zeros(1, pardim);
            vec_depl(uparams(i)) = -1;

            if (((par_current(uparams(i)) + vec_depl(uparams(i))) >= 1) && ((par_current(uparams(i)) + vec_depl(uparams(i))) <= B_size_par))
                par_current + vec_depl;
                res = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current + vec_depl);
                err = eval(res, expdata, timedata);

                if (err < err_current)
                    err_current = err;

                    % vec_mem(i) = vec_mem(i) + vec_depl(i);
                    err_res = err_current;
                    par_best = par_current + vec_depl;
                end

            end

        end

        % par_current = par_best;
        % res_current = simulate_bn(vardim, pardim, ntime, engine, B_size, M, bn, par_current);
        % err_current = eval(res_current, expdata, timedata);
        % err_current_mem = err_current;

        it++

        if norm(par_old - par_best) == 0
            par_res = par_best;
            break

        else

            % res_current = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_best);
            % err_current = eval(res_current, expdata, timedata);
            par_current = par_best;
            err_current = err_res;
            % par_old = par_best;
            % err_res = err_current;
            % err_current_mem = err_current;

            % if err_current < err_current_mem
            %     par_current = par_best;
            %     par_old = par_current;
            %     err_current_mem = err_current;

            % else
            %     par_res = par_current
            %     break

            % end

            %     parr_current = par_best;
            %     res_current = simulate_bn(vardim, pardim, ntime, engine, B_size, M, bn, par_current);
            %     err_current = eval(res_current, expdata, timedata);

        end

    end

    % if

    %     res = simulate_bn(vardim, pardim, ntime, engine, B_size, M, bn, par_best);
    %     err = eval(res, expdata, timedata);

    %     if err < err_current
    %         par_current = par_best;
    %     end

    % if vec_mem == zeros(1, pardim);
    %     par_current
    %     break

    % end
    % end

    err_res = err_current;
    par_res = res = par_best;
    % keyboard
end
