function [var_samples,par_samples] = sample_halton (N, var, par)
  [nvar,_] = size(var);
  [npar,_] = size(par);
  b = list_primes(nvar+npar);

  for i=1:nvar
    low = var(i,2);
    up = var(i,3);
    var_samples(:,i) = halton_sequence(N,b(i))*(up-low) + low;
  endfor
  for i=1:npar
    low = par(i,2);
    up = par(i,3);
    par_samples(:,i) = halton_sequence(N,b(i+nvar))*(up-low) + low;
  endfor
endfunction