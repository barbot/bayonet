
function dbn = mk_dynamic_bayesian(var,par, onodes, dependencyvar,dependencypar, varname, parname, cpd)
  nvar = size(dependencyvar)(1,1);
  npar = size(dependencypar)(1,2);
#keyboard
  dbninter =  [dependencyvar',zeros(nvar,npar);
        dependencypar',eye(npar)];
  dbnintra = zeros(length(parname)+length(varname));
  #observed nodes

% keyboard
  varsize = mk_nodesize(var);
  parsize = mk_nodesize(par);
  nnvar = mk_nodesize(var);
  nnpar = mk_nodesize(par);
  ns = [varsize,parsize]; % binary nodes
 
%onodes = [1 2 3 4 6]
dnodes = [1:(nvar+npar)];
  %dbn=mk_dbn(dbnintra,dbninter, [varsize parsize],'discrete', 1:(nvar+npar),'observed', onodes, 'names', [varname,parname]);
dbn=mk_dbn(dbnintra,dbninter, ns, 'discrete', dnodes, 'observed', onodes, 'names', [varname,parname]);
  for i=1:nvar
    dbn.CPD{i} = tabular_CPD(dbn, i, 1/varsize(i) * ones(varsize(i),1));
  endfor
  for i=1+nvar:nvar+npar
    dbn.CPD{i} = tabular_CPD(dbn, i, 1/parsize(i-nvar) * ones(int32(parsize(i-nvar)),1));
  endfor
  for i=1+nvar+npar:nvar+npar+length(cpd)
    dbn.CPD{i} = tabular_CPD(dbn, i, reshape_transpose(full(cpd{i-nvar-npar}),var,i-nvar-npar));

  endfor
  for i=1+nvar+npar+nvar:nvar+npar+nvar+npar
  
    dbn.CPD{i} = tabular_CPD(dbn, i );
  endfor

endfunction


