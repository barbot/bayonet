function u = halton_sequence (N, base)
  u=double(zeros(N,1));      
  for index= 1:N
        i=index;
        r=0;
        f=1;
        while true
          if not(i>0)
            break;
          end
          f=double(f)./double(base);
          r=r+f.*(mod(i,base));
          i= floor(double(i)./double(base));
        endwhile
        u(index)=r;
  endfor
endfunction