function res = simulate_bn(vardim, pardim, ntime, B_size, M, bn, x_init, param_ev,  oparams, uparams)
    evidence = cell(1, vardim+pardim+vardim);
    soft_evidence = cell(1, vardim+pardim+vardim);
    res = zeros(vardim, ntime);
    % keyboard
    % engine = jtree_inf_engine(bn);
    engine = jtree_inf_engine(bn);
    [engine, loglik] = enter_evidence(engine, x_init);

    for i = 1:pardim
        evidence{vardim + i} = param_ev(i);
    end

    for i = 1:vardim
        #  for b = 1:B_size_par
        #    evidence{vardim+b} = b

        for j = 1:B_size
            res(i, 1) = M(i, x_init{i});
        endfor

    end

    % [engine, loglik] = enter_evidence(engine, evidence);

    for t = 2:ntime

        for i = 1:vardim
            #  for b = 1:B_size_par
            #    evidence{vardim+b} = b
            marg(i) = marginal_nodes(engine, vardim + pardim + i);
% keyboard
            for j = 1:B_size
                res(i, t) += marg(i).T(j) * M(i, j);
            endfor

        end

        engine = jtree_inf_engine(bn);

        for i = 1:vardim
            soft_evidence{i} = marg(i).T;
        end

        [engine, loglik] = enter_evidence(engine, evidence, 'soft', soft_evidence);
    end

% keyboard

end
