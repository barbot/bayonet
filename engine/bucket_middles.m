function [out] = bucket_middles (var, B_size_par)
    [m n] = size(var);

    for i = 1:m

        step = var(i, 4);

        for j = 0:B_size_par - 1
            out(i, j + 1) = var(i, 2) + step / 2 + j * step;
        endfor

    endfor

endfunction
