function node_sizes = mk_nodesize(var) # calcule le nombre d'intervalles par variables/parametre
  [m n] = size(var);
  for i=1:m
    low = var(i,2);
    up = var(i,3);
    step = var(i,4);
    node_sizes(i)=max(1,(up-low)/step);
  endfor
endfunction