function res_err = eval(res, expdata, timedata)
    [t, n] = size(expdata);

    % t = 3;
    res_err = 0;

% keyboard

    for i = 1:t

        for j = 1:n
            % res_err += (res(j, timedata(i)) - expdata(i, j)) * (res(j, timedata(i)) - expdata(i, j));
            res_err += (res(j, i) - expdata(i, j)) * (res(j, i) - expdata(i, j));

        end

    end

end
