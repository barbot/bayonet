function vsim = sim_bayesian_traj(bn, var, par, t  )
[nvar,_] = size(var);
[npar,_] = size(par);

[disc_init_v,_] = discretize(var(:,1)',var);
pv = probvect( var, disc_init_v+1);
for i=1:nvar
    bn.CPD{i} = tabular_CPD(bn, i, pv(i,:));
endfor

[disc_init_p,_] = discretize(par(:,1)',par);
pp = probvect( par, disc_init_p+1);
#keyboard
for i=1:npar
    bn.CPD{nvar+i} = tabular_CPD(bn, nvar+i, pp(i,:));
endfor


for i=1:(length(t)-1)
  x = sample_bnet(bn);
  for j=1:nvar
    if x{nvar+npar+j,1} == 10 
     # keyboard
    endif
    vsim(i,j) = x{j,1};
    pv = probvect( var(j,:), x{nvar+npar+j,1})
    bn.CPD{j} = tabular_CPD(bn, j, pv);
  endfor
endfor
vsim
 
%Affichage des simus continues et discretes
subplot(1,2,1)
hold on
for i = 1:nvar
  stairs(t(1:end-1),vsim(:,i));
endfor
%axis([0 10 0 10])
subplot(1,2,2)
hold on
x = lsode(@(x,t) f(x,t,par(:,1)),var(:,1),t);
for i = 1:nvar
  plot(t,x(:,i));
endfor
%axis([0 10 0 10])
legend("x1","x2", "location", "northeastoutside")


endfunction