function [err_res, par_res] = hooke_jeeves(par_init, x_ev, vardim, pardim, ntime, B_size_par, M, bn, expdata, timedata, oparams, uparams)
    par_current = par_init;
    par_best = par_current;
    par_best_min = par_current;
    vec_depl = zeros(1, pardim);
    vec_mem = zeros(1, pardim);
    it = 0;

    par_current = par_best;
    par_old = par_current;
    res_current = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current);
    err_current = eval(res_current, expdata, timedata);
    err_current_min = err_current;
    err_current_mem = err_current;

    while (true)

        for i = 1:length(uparams)

            vec_depl = zeros(1, pardim);
            vec_depl(uparams(i)) = 1;
            err_current = err_current_mem;

            if (((par_current(uparams(i)) + vec_depl(uparams(i))) >= 1) && ((par_current(uparams(i)) + vec_depl(uparams(i))) <= B_size_par))
                par_current + vec_depl;
                res = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current + vec_depl);
                err = eval(res, expdata, timedata);

                if (err < err_current_min)
                    par_best_min = par_current + vec_depl;
                    err_current_min = err;
                end

                if (err < err_current)
                    par_best = par_best + vec_depl;
                    % err_current = err
                    % vec_mem(i) = vec_mem(i) + vec_depl(i);
                else

                    vec_depl = zeros(1, pardim);
                    vec_depl(uparams(i)) = -1;
                    % keyboard

                    if (((par_current(uparams(i)) + vec_depl(uparams(i))) >= 1) && ((par_current(uparams(i)) + vec_depl(uparams(i))) <= B_size_par))
                        par_current + vec_depl;
                        res = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current + vec_depl);
                        err = eval(res, expdata, timedata);

                        if (err < err_current_min)
                            par_best_min = par_current + vec_depl;
                            err_current_min = err;
                        end

                        if (err < err_current)
                            % vec_mem(i) = vec_mem(i) + vec_depl(i);
                            par_best = par_best + vec_depl;
                            % err_current = err

                        end

                    end

                end

            else

                vec_depl = zeros(1, pardim);
                vec_depl(uparams(i)) = -1;
                % keyboard

                if (((par_current(uparams(i)) + vec_depl(uparams(i))) >= 1) && ((par_current(uparams(i)) + vec_depl(uparams(i))) <= B_size_par))
                    par_current + vec_depl;
                    res = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_current + vec_depl);
                    err = eval(res, expdata, timedata);

                    if (err < err_current_min)
                        par_best_min = par_current + vec_depl;
                        err_current_min = err;
                    end

                    if (err < err_current)
                        % vec_mem(i) = vec_mem(i) + vec_depl(i);
                        par_best = par_best + vec_depl;
                        % err_current = err

                    end

                end

            end

        end

        % par_current = par_best;
        % res_current = simulate_bn(vardim, pardim, ntime, engine, B_size, M, bn, par_current);
        % err_current = eval(res_current, expdata, timedata);
        % err_current_mem = err_current;

        it++

        if norm(par_old - par_best) == 0
            err_res = err_current;

            par_res = par_current;
            break

        else

            res_current = simulate_bn(vardim, pardim, ntime, B_size_par, M, bn, x_ev, par_best);
            err_current = eval(res_current, expdata, timedata);
            err_res = err_current;

            if (err_current < err_current_mem) && (err_current < err_current_min)
                disp("combined move")
                par_current = par_best;
                par_old = par_current;
                par_best_min = par_current;
                err_current_mem = err_current;
                err_current_min = err_current;
                err_res = err_current;

            else

                disp("one move")
                % if norm(par_old - par_best_min) == 0
                %     par_res = par_best_min;
                %     break
                % else
                par_current = par_best_min;
                par_old = par_current;
                par_best = par_best_min;
                err_current_mem = err_current_min;
                err_res = err_current_min;

                % end

                % par_res = par_current;

            end

            %     parr_current = par_best;
            %     res_current = simulate_bn(vardim, pardim, ntime, engine, B_size, M, bn, par_current);
            %     err_current = eval(res_current, expdata, timedata);

        end

        % if

        %     res = simulate_bn(vardim, pardim, ntime, engine, B_size, M, bn, par_best);
        %     err = eval(res, expdata, timedata);

        %     if err < err_current
        %         par_current = par_best;
        %     end

        % if vec_mem == zeros(1, pardim);
        %     par_current
        %     break

        % end
        % end

        err_res = err_current;
        par_res = par_best;
        % keyboard
    end

end
