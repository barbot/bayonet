function vsim = sim_bayesian_traj(dbn, var, par, t  )
[nvar,_] = size(var);
[npar,_] = size(par);

[disc_init_v,_] = discretize(var(:,1)',var);
pv = probvect( var, disc_init_v+1);
for i=1:nvar
    dbn.CPD{i} = tabular_CPD(dbn, i, pv(i,:));
endfor

[disc_init_p,_] = discretize(par(:,1)',par);
pp = probvect( par, disc_init_p+1);
#keyboard
for i=1:npar
    dbn.CPD{nvar+i} = tabular_CPD(dbn, nvar+i, pp(i,:));
endfor


xx = sample_dbn(dbn,10);
vsim = [];
int = [];
for i = 1:size(xx)(1)
  for j = 1:size(xx)(2)
    int = [int,xx{i,j}];
  endfor
  vsim = [vsim;int];
  int = [];
endfor

% keyboard
%Affichage des simus continues et discretes
subplot(1,2,1)
hold on
for i = 1:nvar
  stairs(t(1:end-1),vsim(i,:));
endfor
%axis([0 10 0 10])
subplot(1,2,2)
hold on
x = lsode(@(x,t) f(x,t,par(:,1)),var(:,1),t);
for i = 1:nvar
  plot(t,x(:,i));
endfor
%axis([0 10 0 10])
legend("x1","x2", "location", "northeastoutside")



endfunction