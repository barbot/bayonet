%EGF sim

warning("off");
addpath("../bnt");
addpath(genpathKPM("../bnt"));
addpath("../engine");

args = argv();
#NB_samples = str2num(args(1){1});
#B_size = str2num(args(2){1});
NB_samples = 3000;
B_size = 3;

varname = {"x", "y", "z", "w"};
parname = {"k1", "k2", "k3"};

global nb_var = 4;
global nb_par = 3;

function xdot = f (x, t, k)
    xdot(1) = -k(1) * x(1) * x(2) + k(2) * x(3);
    xdot(2) = -k(1) * x(1) * x(2) + (k(2) + k(3)) * x(3);
    xdot(3) = k(1) * x(1) * x(2) - (k(2) + k(3)) * x(3);
    xdot(4) = k(3) * x(3);
endfunction

% dependencypar = [ [ 1 1 0]  %x1 depends on k1 and k2
%                   [ 1 1 1]
%                   [ 1 1 1]
%                   [ 0 0 1]];
% dependencyvar = [ [ 1 1 1 0]  %x1 depends on x1, x2 and x3
%                   [ 1 1 1 0]
%                   [ 1 1 1 0]
%                   [ 0 0 1 1]];

dependencypar = [[1 1 1]%x1 depends on k1 and k2
                [1 1 1]
                [1 1 1]
                [1 1 1]];
dependencyvar = [[1 1 1 0]%x1 depends on x1, x2 and x3
                [1 1 1 0]
                [1 1 1 0]
                [1 1 1 1]];

t = linspace(0, 10, 11);
%t = linspace(0,1,2);

%Compute sim %%%%%%

% global timeode=0;
% global timemapid=0;
% id = tic();
%  bncpd = gen_bncpd(var,par,dependencyvar, dependencypar,t,NB_samples);
% toc(id)
% timeode
% timemapid

% for NB_samples = NB_samples_vec
%   for B_size = B_size_vec
name = strcat("./CPDs/bayesian_cpd_", num2str(NB_samples), "_b", num2str(B_size));
load(name, "bncpd");
var = [
    9 0 10 10 / B_size; %6
    1 0 10 10 / B_size; %5
    1.5 0 15 15 / B_size; %10
    1.5 0 15 15 / B_size; %5
    ];
par = [
    0.1 0 1 1; %10
    0.1 0 1 1; %4
    0.3 0 1 1; %5
    ];

bn = mk_static_bayesian(var, par, dependencyvar, dependencypar, varname, parname, bncpd);

%   endfor
% endfor

#FIXED PARAMETERS TO RECOVER
k = [0.9, 0.5, 0.1];
[nk, _] = discretize(k, par);
nk++;
evidence = cell(1, 11);

x0 = [9; 9; 4.5; 1.5]; %var(:,1);%%
fk = @(x, t) f(x, t, k);
id = tic();
x = lsode(fk, x0, t);
[nt, nx] = size(x);

N = 11;
tsample = 1;
nsamples = nt - 1;
nsimus = 50;
samples = cell(N, nsamples / tsample * nsimus);

plot(t, x);
h = legend ("x1", "x2", "x3", "x4");
legend (h, "location", "northeastoutside");
set (h, "fontsize", 20);
#keyboard
cpt = 1;

for s = 1:nsimus
    s
    x0 = [10 * rand(); 10 * rand(); 5 * rand(); 5 * rand()];
    x = lsode(fk, x0, t);

    for i = 1:tsample:10
        xd = discretize(x(i, 1:4), var) +1; % /!\ discretize numerote à partir de zero /!\
        samples(1, cpt) = xd(1);
        samples(2, cpt) = xd(2);
        samples(3, cpt) = xd(3);
        samples(4, cpt) = xd(4);

        samples(5, cpt) = nk(1);

        xdtp1 = discretize(x(i + 1, 1:4), var) +1;
        samples(8, cpt) = xdtp1(1);
        samples(9, cpt) = xdtp1(2);
        samples(10, cpt) = xdtp1(3);
        samples(11, cpt) = xdtp1(4);
        cpt++;
    endfor

endfor

% addpath("../../bnt");
% addpath(genpathKPM("../../bnt"));
% load("bayesian_cpd.mat");

#bn2 = mk_static_bayesian(var,par,dependencyvar,dependencypar, varname, parname, []);

% bn = mk_static_bayesian(var,par,dependencyvar,dependencypar, varname, parname, bncpd);

engine = jtree_inf_engine(bn);
% [engine, loglik] = enter_evidence(engine, evidence);
% evidence = cell(1,N);
% evidence{6} = 3;
% [engine, loglik] = enter_evidence(engine, evidence);

max_iter = 10;
bn2 = learn_params_em(engine, samples, max_iter);

CPTl = cell(1, N);

for i = 1:N
    s = struct(bn2.CPD{i}); % violate object privacy
    CPTl{i} = s.CPT;
end

dispcpt(CPTl{5})
dispcpt(CPTl{6})
dispcpt(CPTl{7})

keyboard

% %build bnet %%%%%
% addpath("../../bnt");
% addpath(genpathKPM("../../bnt"));
% load("bayesian_cpd.mat");
% bn = mk_static_bayesian(var,par,dependencyvar,dependencypar, varname, parname, bncpd);

% dbn = mk_dynamic_bayesian(var,par,dependencyvar,dependencypar, varname, parname, bncpd);

% ### INFERENCE

% # for static Bnet:  engine = jtree_inf_engine(bnet);
% #engine = smoother_engine(hmm_2TBN_inf_engine(dbn));
% engine = jtree_inf_engine(bn);
% #engine = smoother_engine(jtree_2TBN_inf_engine(dbn));
% #ev = sample_dbn(dbn,2);
% evidence = cell(1,11);
% [engine, loglik] = enter_evidence(engine, evidence);
% marg = marginal_nodes(engine, 8);

% keyboard

% #evidence = cell(7,2);

% # APPRENTISSAGE DE PARAMETRES
% N = 11;
% nsamples = 500;
% samples = cell(N, nsamples);
% for i=1:nsamples
%   samples(:,i) = sample_bnet(bn);
% end

% % Make a tabula rasa
% %bnet2 = mk_bnet(dag_egf,[mk_nodesize(var) mk_nodesize(par) mk_nodesize(var)], 'names', {'x1','x2','x3','x4','k1','k2','k3','x1p','x2p','x3p','x4p'});
% bnet2 = mk_static_bayesian(var,par,dependencyvar,dependencypar, varname, parname, []);
% seed = 0;
% rand('state', seed);
% bnet2.CPD{1} = tabular_CPD(bn, 1);
% bnet2.CPD{2} = tabular_CPD(bn, 2);
% bnet2.CPD{3} = tabular_CPD(bn, 3);
% bnet2.CPD{4} = tabular_CPD(bn, 4);
% bnet2.CPD{5} = tabular_CPD(bn, 5);
% bnet2.CPD{6} = tabular_CPD(bn, 6);
% bnet2.CPD{7} = tabular_CPD(bn, 7);
% bnet2.CPD{8} = tabular_CPD(bn, 8);
% bnet2.CPD{9} = tabular_CPD(bn, 9);
% bnet2.CPD{10} = tabular_CPD(bn, 10);
% bnet2.CPD{11} = tabular_CPD(bn, 11);

% bnet3 = learn_params(bnet2, samples);

% CPT3 = cell(1,N);
% for i=1:N
%   s=struct(bnet3.CPD{i});  % violate object privacy
%   CPT3{i}=s.CPT;
% end
% dispcpt(CPT3{4})

% keyboard
% # APPRENTISSAGE PARAMETRES AVEC DONNEES INCOMPLETES

% samples2 = samples;
% hide = rand(N, nsamples) > 0.5;
% [I,J]=find(hide);
% for k=1:length(I)
%   samples2{I(k), J(k)} = [];
% end

% engine2 = jtree_inf_engine(bnet2);
% max_iter = 2;
% [bnet4, LLtrace] = learn_params_em(engine2, samples2, max_iter);

% CPT4 = cell(1,N);
% for i=1:N
%   s=struct(bnet4.CPD{i});  % violate object privacy
%   CPT4{i}=s.CPT;
% end
% dispcpt(CPT4{4})

% keyboard
