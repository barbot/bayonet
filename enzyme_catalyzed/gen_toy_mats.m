%EGF sim

warning("off");

ntime = 51;

args = argv();
NB_samples = str2num(args(1){1});
B_size = str2num(args(2){1});
B_size_par = str2num(args(3){1});
con = args(4){1};

#keyboard

var = [
    5 0 10 10 / B_size; %6
    1 0 10 10 / B_size; %5
    7.5 0 15 15 / B_size; %10
    1.5 0 15 15 / B_size; %5
    ];

vardim = length(var(:, 1));

varname = {"x", "y", "z", "w"};

par = [
    0.3 0 1 1 / B_size_par; %10
    0.3 0 1 1 / B_size_par; %4
    0.3 0 1 1 / B_size_par; %5
    ];

pardim = length(par(:, 1));

%par = [
%  0.1 0.09 0.11 0.02/B_size_par; %10
%  0.1 0.09 0.11 0.02/B_size_par; %4
%  0.3 0.29 0.31 0.02/B_size_par; %5
%];

parname = {"k1", "k2", "k3"};

global nb_var = 4;
global nb_par = 3;

function xdot = f (x, t, k)
    xdot(1) = -k(1) * x(1) * x(2) + k(2) * x(3);
    xdot(2) = -k(1) * x(1) * x(2) + (k(2) + k(3)) * x(3);
    xdot(3) = k(1) * x(1) * x(2) - (k(2) + k(3)) * x(3);
    xdot(4) = k(3) * x(3);
endfunction

if strcmp(con, "euler")
    dependencypar = [[1 1 0]%x1 depends on k1 and k2
                [1 1 1]
                [1 1 1]
                [0 0 1]];
    dependencyvar = [[1 1 1 0]%x1 depends on x1, x2 and x3
                [1 1 1 0]
                [1 1 1 0]
                [0 0 1 1]];
elseif strcmp(con, "rk4")
    dependencypar = [[1 1 1]%x1 depends on k1 and k2
                    [1 1 1]
                    [1 1 1]
                    [1 1 1]];
    dependencyvar = [[1 1 1 0]%x1 depends on x1, x2 and x3
                    [1 1 1 0]
                    [1 1 1 0]
                    [1 1 1 1]];

else
    disp("wrong argument")
endif

t = linspace(0, 1, 2);

addpath("../engine");

% x = lsode(@(x,t) f(x,t,par(:,1)),var(:,1),t);
% plot(t,x);
% legend ({"x1", "x2","x3","x4"}, "location", "east");

%Compute sim %%%%%%

global timeode = 0;
global timemapid = 0;

% profile on
tic()
bncpd = gen_bncpd_nomap(var, par, dependencyvar, dependencypar, t, NB_samples, true);
comp_time = toc()

% profile off
% T = profile("info")
% profshow(T)
suffixe = "";

if strcmp(con, "euler")
    suffixe="_noncon";
elseif strcmp(con, "rk4")
    suffixe="_con";
else
    disp("wrong argument")
endif

name = strcat("./CPDs/bayesian_cpd_", num2str(NB_samples), "_b", num2str(B_size), suffixe);
save(name, "bncpd");

keyboard

% %build bnet %%%%%
% addpath("../../bnt");
% addpath(genpathKPM("../../bnt"));
% load("./CPDs/bayesian_cpd_100000_b5_noncon");

% bn = mk_static_bayesian(var, par, dependencyvar, dependencypar, varname, parname, bncpd);

% sim_bayesian_traj(bn, var, par, t2);
