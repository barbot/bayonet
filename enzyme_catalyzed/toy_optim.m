%EGF sim

warning("off");
pkg load statistics;

ntime = 11;

args = argv();
NB_samples = str2num(args(1){1});
B_size = str2num(args(2){1});
B_size_par = str2num(args(3){1});
con = args(4){1};

#keyboard

var = [
    7 0 10 10 / B_size; %6
    3 0 10 10 / B_size; %5
    5.5 0 15 15 / B_size; %10
    1.5 0 15 15 / B_size; %5
    ];

vardim = length(var(:, 1));

varname = {"x", "y", "z", "w"};

par = [
    0.9 0 1 1 / B_size_par; %10
    0.9 0 1 1 / B_size_par; %4
    0.9 0 1 1 / B_size_par; %5
    ];

pardim = length(par(:, 1));

%par = [
%  0.1 0.09 0.11 0.02/B_size_par; %10
%  0.1 0.09 0.11 0.02/B_size_par; %4
%  0.3 0.29 0.31 0.02/B_size_par; %5
%];

parname = {"k1", "k2", "k3"};

global nb_var = 4;
global nb_par = 3;

function xdot = f (x, t, k)
    xdot(1) = -k(1) * x(1) * x(2) + k(2) * x(3);
    xdot(2) = -k(1) * x(1) * x(2) + (k(2) + k(3)) * x(3);
    xdot(3) = k(1) * x(1) * x(2) - (k(2) + k(3)) * x(3);
    xdot(4) = k(3) * x(3);
endfunction

if strcmp(con, "euler")
    dependencypar = [[1 1 0]%x1 depends on k1 and k2
                [1 1 1]
                [1 1 1]
                [0 0 1]];
    dependencyvar = [[1 1 1 0]%x1 depends on x1, x2 and x3
                [1 1 1 0]
                [1 1 1 0]
                [0 0 1 1]];
elseif strcmp(con, "rk4")
    dependencypar = [[1 1 1]%x1 depends on k1 and k2
                [1 1 1]
                [1 1 1]
                [1 1 1]];
    dependencyvar = [[1 1 1 0]%x1 depends on x1, x2 and x3
                [1 1 1 0]
                [1 1 1 0]
                [1 1 1 1]];
else
    disp("wrong argument")
endif

t2 = linspace(0, 10, 11);

addpath("../engine");

% x = lsode(@(x,t) f(x,t,par(:,1)),var(:,1),t);
% plot(t,x);
% legend ({"x1", "x2","x3","x4"}, "location", "east");

%Compute sim %%%%%%

global timeode = 0;
global timemapid = 0;

#bncpd = gen_bncpd(var,par,dependencyvar, dependencypar,t,NB_samples,true);

suffixe = ""

if strcmp(con, "euler")
    suffixe="_noncon"
elseif strcmp(con, "rk4")
    suffixe="_con"
else
    disp("wrong argument")
endif

name = strcat("./CPDs/bayesian_cpd_", num2str(NB_samples), "_b", num2str(B_size), suffixe);
# save(name, "bncpd");

% keyboard

%build bnet %%%%%
addpath("../bnt");
addpath(genpathKPM("../bnt"));
% load("./CPDs/bayesian_cpd_50000_b5_con");
load(name);
bn = mk_static_bayesian(var, par, dependencyvar, dependencypar, varname, parname, bncpd);

% sim_bayesian_traj(bn, var, par, t2);

% dbn = mk_dynamic_bayesian(var,par,dependencyvar,dependencypar, varname, parname, bncpd);

% ### INFERENCE

% # for static Bnet:  engine = jtree_inf_engine(bnet);
% #engine = smoother_engine(hmm_2TBN_inf_engine(dbn));

% #engine = smoother_engine(jtree_2TBN_inf_engine(dbn));
% #ev = sample_dbn(dbn,2);

x0 = var(:, 1)'; #whatever

% M = [1.0, 3.0, 5.0, 7.0, 9.0; 1.0, 3.0, 5.0, 7.0, 9.0; 1.5, 4.5, 7.5, 10.5, 13.5; 1.5, 4.5, 7.5, 10.5, 13.5];

M = bucket_middles(var, B_size_par);
% keyboard
% M = [1.0, 3.0, 5.0, 7.0, 9.0; 1.0, 3.0, 5.0, 7.0, 9.0; 1.5, 4.5, 7.5, 10.5, 13.5; 1.5, 4.5, 7.5, 10.5, 13.5];

% expdata = [9	1	13.5	1.5; 4.47739	5.52261	7.76633	2.71106; 4.06442	5.93558	5.60208	4.46234; 4.36478	5.63522	4.15852	6.20627; 4.8746	5.1254	3.05119	7.82341; 5.46711	4.53289	2.19369	9.27342; 6.08826	3.91174	1.54794	10.5403; 6.70037	3.29963	1.07898	11.6214; 7.27339	2.72661	0.749351	12.524; 7.78627	2.21373	0.522868	13.2634; 8.2282	1.7718	0.368801	13.8594];
timedata = [1, 11, 21, 31, 41, 51, 61, 71, 81, 91, 101];

evidence = cell(1, 11);

x_init = discretize(x0, var);

for i = 1:vardim
    evidence{i} = x_init(i) + 1;
end

% [engine, loglik] = enter_evidence(engine, evidence);

tab_res = [];

t = linspace(0, 10, 101);

x = lsode(@(x, t) f(x, t, par(:, 1)), var(:, 1), t);
plot(t, x);
legend ({"x1", "x2", "x3", "x4"}, "location", "east");

expdata = x(timedata, :);

% keyboard

uparams = [1 2 3];
oparams = [];
par_init = [4, 4, 4];

%% Setting initial parameters for evolution strategy
% n_x = 3;
% func = @(x, u) (x(1, :).^2 + x(2, :).^2 - 2 * x(1, :)).^2 + x(1, :) / 4;
n_x = 3; % 'n_x' states
limits = repmat([1 5], n_x, 1); % Boundaries
obj = 0; % objective value (f(x_min) = obj)

nf = 1; % length of the output vector 'f(x,y)'
mu = 5; % parent population size
lambda = 5; % offspring population size
gen = 15; % number of generations
sel = '+'; % Selection scheme (Pag. 78 in (BACK))
rec_obj = 2; % Type of recombination to use on object
% variables (Pag. 74 in (BACK))
% See 'recombination.m'
rec_str = 4; % Type of recombination to use on strategy
% parameters (Pag. 74 in (BACK))
u = 0; % external excitation

% disp("SRES")
% tic()
% [min_x, min_f, off, EPS, idx] = evolution_strategy_discrete(mu, lambda, gen, sel, rec_obj, rec_str, u, obj, nf, n_x, limits, vardim, pardim, ntime, B_size_par, M, bn, expdata, timedata, evidence);
% toc()
% disp ("Hooke jeeves std")
% tic()
% [err_res, res] = hooke_jeeves(par_init, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()
% disp ("Hooke jeeves min of star")
% tic()
% [err_res2, res2] = hooke_jeeves_min(par_init, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()
% disp ("Hooke jeeves modified")
% tic()
% [err_res3, res3] = hooke_jeeves_mod(par_init, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()
% % % keyboard
% disp ("Inference search")
% tic()
% [err_res5, res5] = inference_search(var, par, t, bn, B_size, expdata, timedata, vardim, pardim, ntime, B_size_par, M, evidence, oparams, uparams);
% toc()

disp("naive search")
tic()
[err_res4, res4] = naive_search(evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata);
toc()

% % % % keyboard

% disp ("Hooke jeeves modified with inference init")
% tic()
% [err_res6, res6] = hooke_jeeves_mod(res5, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()
% disp ("Hooke jeeves min with inference init")
% tic()
% [err_res7, res7] = hooke_jeeves_min(res5, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()
% load("tab_res_7.mat", "tab_res");

keyboard
% keyboard
#for i = 1:10
#  for j = 1:4
#    err = result[X[j]][T[i]] - expdata[i][j];
#    value += weights[j] * err * err;
#  end
#end

keyboard
% #evidence = cell(7,2);

% # APPRENTISSAGE DE PARAMETRES
% N = 11;
% nsamples = 500;
% samples = cell(N, nsamples);
% for i=1:nsamples
%   samples(:,i) = sample_bnet(bn);
% end

% % Make a tabula rasa
% %bnet2 = mk_bnet(dag_egf,[mk_nodesize(var) mk_nodesize(par) mk_nodesize(var)], 'names', {'x1','x2','x3','x4','k1','k2','k3','x1p','x2p','x3p','x4p'});
% bnet2 = mk_static_bayesian(var,par,dependencyvar,dependencypar, varname, parname, []);
% seed = 0;
% rand('state', seed);
% bnet2.CPD{1} = tabular_CPD(bn, 1);
% bnet2.CPD{2} = tabular_CPD(bn, 2);
% bnet2.CPD{3} = tabular_CPD(bn, 3);
% bnet2.CPD{4} = tabular_CPD(bn, 4);
% bnet2.CPD{5} = tabular_CPD(bn, 5);
% bnet2.CPD{6} = tabular_CPD(bn, 6);
% bnet2.CPD{7} = tabular_CPD(bn, 7);
% bnet2.CPD{8} = tabular_CPD(bn, 8);
% bnet2.CPD{9} = tabular_CPD(bn, 9);
% bnet2.CPD{10} = tabular_CPD(bn, 10);
% bnet2.CPD{11} = tabular_CPD(bn, 11);

% bnet3 = learn_params(bnet2, samples);

% CPT3 = cell(1,N);
% for i=1:N
%   s=struct(bnet3.CPD{i});  % violate object privacy
%   CPT3{i}=s.CPT;
% end
% dispcpt(CPT3{4})

% keyboard
% # APPRENTISSAGE PARAMETRES AVEC DONNEES INCOMPLETES

% samples2 = samples;
% hide = rand(N, nsamples) > 0.5;
% [I,J]=find(hide);
% for k=1:length(I)
%   samples2{I(k), J(k)} = [];
% end

% engine2 = jtree_inf_engine(bnet2);
% max_iter = 2;
% [bnet4, LLtrace] = learn_params_em(engine2, samples2, max_iter);

% CPT4 = cell(1,N);
% for i=1:N
%   s=struct(bnet4.CPD{i});  % violate object privacy
%   CPT4{i}=s.CPT;
% end
% dispcpt(CPT4{4})

% keyboard
