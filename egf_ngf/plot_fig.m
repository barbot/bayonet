
b_size = 5;

t = linspace(0, 100, 101);

var_init = var(:,1);

for i = 1:length(var)
    var_init(i) = var(i,2) + 0.5*(var(i,3)-var(i,2));
endfor


par_init = k;
vec_middles = zeros(b_size,1);
for i = 1:b_size
    vec_middles(i) = 1/(2*b_size) + (i-1)*1/b_size;
endfor

par_best = par(:,2) + par(:,4).*vec_middles(res5);

x = lsode(@(x, t) f(x, t, par_init), var_init, t);
x_learned = lsode(@(x, t) f(x, t, par_best), var_init, t);

clf
format short eng 
  % set the new ticklabels to the plot

subplot(3,2,1);
    variable=22
    l1 = plot(t, x(:,variable), "r"); 
    hold on;
    l2 = plot(t,x_learned(:,variable), "--r");
    xlabel("Time")
    ylabel("PI3K*")
    set(l1,"linewidth",2);
    set(l2,"linewidth",2);
    set(gca, "linewidth", 2, "fontsize", 10)
    ylab=linspace(0,var(variable,3),6);
    set(gca, 'YTick', ylab)
    % set(gca,'YTick',ylab)
    set(gca,'YTickLabel',strsplit(sprintf('%1.0e\n',ylab), "\n"))
    axis([t(1) t(end) var(variable,2) var(variable,3)])

subplot(3,2,2);
    variable=5
    l1 = plot(t, x(:,variable), "g"); 
    hold on
    l2 = plot(t,x_learned(:,variable), "--g");
    xlabel("Time")
    ylabel("freeNGFR")
    set(l1,"linewidth",2);
    set(l2,"linewidth",2);
    set(gca, "linewidth", 2, "fontsize", 10)
    ylab=linspace(0,var(variable,3),6);
    set(gca, 'YTick', ylab)
    % set(gca,'YTick',ylab)
    set(gca,'YTickLabel',strsplit(sprintf('%1.0e\n',ylab), "\n"))
    axis([t(1) t(end) var(variable,2) var(variable,3)])

subplot(3,2,3);
    variable=18
    l1 = plot(t, x(:,variable), "b"); 
    hold on
    l2 = plot(t,x_learned(:,variable), "--b");
    xlabel("Time")
    ylabel("MEK*")
    set(l1,"linewidth",2);
    set(l2,"linewidth",2);
    set(gca, "linewidth", 2, "fontsize", 10)
    ylab=linspace(0,var(variable,3),6);
    set(gca, 'YTick', ylab)
    % set(gca,'YTick',ylab)
    set(gca,'YTickLabel',strsplit(sprintf('%1.0e\n',ylab), "\n"))
    axis([t(1) t(end) var(variable,2) var(variable,3)])

subplot(3,2,4);
    variable = 20
    l1 = plot(t, x(:,variable), "m"); 
    hold on
    l2 = plot(t,x_learned(:,variable), "--m");
    xlabel("Time")
    ylabel("ERK*") 
    set(l1,"linewidth",2);
    set(l2,"linewidth",2);
    set(gca, "linewidth", 2, "fontsize", 10)
    ylab=linspace(0,var(variable,3),6);
    set(gca, 'YTick', ylab)
    % set(gca,'YTick',ylab)
    set(gca,'YTickLabel',strsplit(sprintf('%1.0e\n',ylab), "\n"))
    axis([t(1) t(end) var(variable,2) var(variable,3)])

subplot(3,2,5);
    variable = 7
    l1 = plot(t, x(:,variable), "c"); 
    hold on
    l2 = plot(t,x_learned(:,variable), "--c");
    xlabel("Time")
    ylabel("SOS*") 
    set(l1,"linewidth",2);
    set(l2,"linewidth",2);
    set(gca, "linewidth", 2, "fontsize", 10)
    ylab=linspace(0,var(variable,3),6);
    set(gca, 'YTick', ylab)
    % set(gca,'YTick',ylab)
    set(gca,'YTickLabel',strsplit(sprintf('%1.0e\n',ylab), "\n"))
    axis([t(1) t(end) var(variable,2) var(variable,3)])

subplot(3,2,6);
    variable = 24
    l1 = plot(t, x(:,variable), "y"); 
    hold on
    l2 = plot(t,x_learned(:,variable), "--y");
    xlabel("Time")
    ylabel("AKT*") 
    set(l1,"linewidth",2);
    set(l2,"linewidth",2);
    set(gca, "linewidth", 2, "fontsize", 10)
    ylab=linspace(0,var(variable,3),6);
    set(gca, 'YTick', ylab)
    % set(gca,'YTick',ylab)
    set(gca,'YTickLabel',strsplit(sprintf('%1.0e\n',ylab), "\n"))
    axis([t(1) t(end) var(variable,2) var(variable,3)])