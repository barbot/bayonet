%EGF sim
warning("off");
addpath("../engine");

NB_samples = 10; #nombre de trajectoires pour générer les CPT
%bucket_size = 5;

var = csvread("var.csv");
var(1, :) = [];
var(:, 1) = [];
var(:, end) = [];

for i = 1:length(var)
    var(i, 4) = (var(i, 3) - var(i, 2)) / var(i, 4);
endfor

par = csvread("par.csv");
par(1, :) = [];
par(:, 1) = [];
par(:, end) = [];

for i = 1:length(par)
    par(i, 4) = (par(i, 3) - par(i, 2)) / par(i, 4);
endfor

function xdot = f (x, t, k)
    xdot(1)=-k(1) * x(1) * x(3) + k(2) * x(4);
    xdot(2)=-k(3) * x(2) * x(5) + k(4) * x(6);
    xdot(3)=-k(1) * x(1) * x(3) + k(2) * x(4);
    xdot(4) = k(1) * x(1) * x(3) - k(2) * x(4);
    xdot(5)=-k(3) * x(2) * x(5) + k(4) * x(6);
    xdot(6) = k(3) * x(2) * x(5) - k(4) * x(6);
    xdot(7) = k(9) * x(10) * x(8) / (x(8) + k(10)) - k(5) * x(4) * x(7) / (x(7) + k(6)) - k(7) * x(6) * x(7) / (x(7) + k(8));
    xdot(8)=-k(9) * x(10) * x(8) / (x(8) + k(10)) + k(5) * x(4) * x(7) / (x(7) + k(6)) + k(7) * x(6) * x(7) / (x(7) + k(8));
    xdot(9)=-k(27) * x(21) * x(9) / (x(9) + k(28));
    xdot(10) = k(27) * x(21) * x(9) / (x(9) + k(28));
    xdot(11)=-k(11) * x(8) * x(11) / (x(11) + k(12)) + k(13) * x(13) * x(12) / (x(12) + k(14));
    xdot(12) = k(11) * x(8) * x(11) / (x(11) + k(12)) - k(13) * x(13) * x(12) / (x(12) + k(14));
    xdot(13) = 0;
    xdot(14)=-k(15) * x(12) * x(14) / (x(14) + k(16)) + k(45) * x(32) * x(15) / (x(15) + k(46)) + k(35) * x(25) * x(15) / (x(15) + k(36));
    xdot(15) = k(15) * x(12) * x(14) / (x(14) + k(16)) - k(45) * x(32) * x(15) / (x(15) + k(46)) - k(35) * x(25) * x(15) / (x(15) + k(36));
    xdot(16)=-k(43) * x(29) * x(16) / (x(16) + k(44)) + k(47) * x(32) * x(17) / (x(17) + k(20)); %k(48) ???
    xdot(17) = k(43) * x(29) * x(16) / (x(16) + k(44)) - k(47) * x(32) * x(17) / (x(17) + k(20));
    xdot(18)=-k(17) * x(15) * x(18) / (x(18) + k(18)) - k(19) * x(17) * x(18) / (x(18) + k(48)) + k(21) * x(31) * x(19) / (x(19) + k(22));
    xdot(19) = k(17) * x(15) * x(18) / (x(18) + k(18)) + k(19) * x(17) * x(18) / (x(18) + k(48)) - k(21) * x(31) * x(19) / (x(19) + k(22));
    xdot(20)=-k(23) * x(19) * x(20) / (x(20) + k(24)) + k(25) * x(31) * x(21) / (x(21) + k(26));
    xdot(21) = k(23) * x(19) * x(20) / (x(20) + k(24)) - k(25) * x(31) * x(21) / (x(21) + k(26));
    xdot(22)=-k(29) * x(4) * x(22) / (x(22) + k(30)) - k(31) * x(12) * x(22) / (x(22) + k(32));
    xdot(23) = k(29) * x(4) * x(22) / (x(22) + k(30)) + k(31) * x(12) * x(22) / (x(22) + k(32));
    xdot(24)=-k(33) * x(23) * x(24) / (x(24) + k(34));
    xdot(25) = k(33) * x(23) * x(24) / (x(24) + k(34));
    xdot(26)=-k(37) * x(6) * x(26) / (x(26) + k(38));
    xdot(27) = k(37) * x(6) * x(26) / (x(26) + k(38));
    xdot(28)=-k(39) * x(27) * x(28) / (x(28) + k(40)) + k(41) * x(30) * x(29) / (x(29) + k(42));
    xdot(29) = k(39) * x(27) * x(28) / (x(28) + k(40)) - k(41) * x(30) * x(29) / (x(29) + k(42));
    xdot(30) = 0;
    xdot(31) = 0;
    xdot(32) = 0;
endfunction

varname = {"x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", "x12", "x13", "x14", "x15", "x16", "x17", "x18", "x19", "x20", "x21", "x22", "x23", "x24", "x25", "x26", "x27", "x28", "x29", "x30", "x31", "x32"};

parname = {"k1", "k2", "k3", "k4", "k5", "k6", "k7", "k8", "k9", "k10", "k11", "k12", "k13", "k14", "k15", "k16", "k17", "k18", "k19", "k20", "k21", "k22", "k23", "k24", "k25", "k26", "k27", "k28", "k29", "k30", "k31", "k32", "k33", "k34", "k35", "k36", "k37", "k38", "k39", "k40", "k41", "k42", "k43", "k44", "k45", "k46", "k47", "k48"};

nb_var = 32;
nb_par = 48;

dependencypar = zeros(nb_var, nb_par);
dependencypar(1, [1 2]) = 1;
dependencypar(2, [3 4]) = 1;
dependencypar(3, [1 2]) = 1;
dependencypar(4, [1 2]) = 1;
dependencypar(5, [3 4]) = 1;
dependencypar(6, [3 4]) = 1;
dependencypar(7, [5 6 7 8 9 10]) = 1;
dependencypar(8, [5 6 7 8 9 10]) = 1;
dependencypar(9, [27 28]) = 1;
dependencypar(10, [27 28]) = 1;
dependencypar(11, [11 12 13 14]) = 1;
dependencypar(12, [11 12 13 14]) = 1;
#dependencypar(13,[])= 1; #pas de parametre
dependencypar(14, [15 16 35 36 45 46]) = 1;
dependencypar(15, [15 16 35 36 45 46]) = 1;
dependencypar(16, [20 43 44 47]) = 1;
dependencypar(17, [20 43 44 47]) = 1;
dependencypar(18, [17 18 19 21 22 48]) = 1;
dependencypar(19, [17 18 19 21 22 48]) = 1;
dependencypar(20, [23 24 25 26]) = 1;
dependencypar(21, [23 24 25 26]) = 1;
dependencypar(22, [29 30 31 32]) = 1;
dependencypar(23, [29 30 31 32]) = 1;
dependencypar(24, [33 34]) = 1;
dependencypar(25, [33 34]) = 1;
dependencypar(26, [37 38]) = 1;
dependencypar(27, [37 38]) = 1;
dependencypar(28, [39 40 41 42]) = 1;
dependencypar(29, [39 40 41 42]) = 1;
#dependencypar(30,[])= 1;
#dependencypar(31,[])= 1;
#dependencypar(32,[])= 1;

dependencyvar = zeros(nb_var, nb_var);
dependencyvar(1, [1 3 4]) = 1;
dependencyvar(2, [2 5 6]) = 1;
dependencyvar(3, [1 3 4]) = 1;
dependencyvar(4, [1 3 4]) = 1;
dependencyvar(5, [2 5 6]) = 1;
dependencyvar(6, [2 5 6]) = 1;
dependencyvar(7, [4 6 7 8 10]) = 1;
dependencyvar(8, [4 6 7 8 10]) = 1;
dependencyvar(9, [9 21]) = 1;
dependencyvar(10, [9 10 21]) = 1; #ajout 10
dependencyvar(11, [11 12 13]) = 1;
dependencyvar(12, [11 12 13]) = 1;
dependencyvar(13, [13]) = 1;
dependencyvar(14, [12 14 15 25 32]) = 1;
dependencyvar(15, [12 14 15 25 32]) = 1;
dependencyvar(16, [16 17 29 32]) = 1;
dependencyvar(17, [16 17 29 32]) = 1;
dependencyvar(18, [15 17 18 19 31]) = 1;
dependencyvar(19, [15 17 18 19 31]) = 1;
dependencyvar(20, [19 20 21 31]) = 1;
dependencyvar(21, [19 20 21 31]) = 1;
dependencyvar(22, [4 12 22]) = 1;
dependencyvar(23, [4 12 22 23]) = 1; #ajout 23
dependencyvar(24, [23 24]) = 1;
dependencyvar(25, [23 24 25]) = 1; #ajout 25
dependencyvar(26, [6 26]) = 1;
dependencyvar(27, [6 26 27]) = 1; #ajout 27
dependencyvar(28, [27 28 29 30]) = 1;
dependencyvar(29, [27 28 29 30]) = 1;
dependencyvar(30, [30]) = 1;
dependencyvar(31, [31]) = 1;
dependencyvar(32, [32]) = 1;

% t = linspace(0,10,101);

%Compute sim %%%%%%

% global timeode=0;
% global timemapid=0;
% id = tic();
% bncpd = gen_bncpd(var,par,dependencyvar, dependencypar,t,NB_samples,true);
% toc(id)
% timeode
% timemapid

% save("./CPDs/bayesian_cpd_50.mat", "bncpd");

%build bnet %%%%%
addpath("../bnt");
addpath(genpathKPM("../bnt"));
load("./CPDs/bayesian_cpd");

onodes = [11 12 14 15 16 17 18 19 20 21 24 25];

bn = mk_static_bayesian(var, par, dependencyvar, dependencypar, varname, parname, bncpd);
dbn = mk_dynamic_bayesian(var, par, onodes, dependencyvar, dependencypar, varname, parname, bncpd);
% ### INFERENCE
% keyboard

engine = jtree_inf_engine(bn);
dengine = smoother_engine(jtree_2TBN_inf_engine(dbn));
%dengine = smoother_engine(hmm_2TBN_inf_engine(dbn));

k = par(:, 1)';
[nk, _] = discretize(k, par);
nk++;

t = linspace(0, 10, 101);

x0 = var(:, 1)'; %var(:,1);%%
fk = @(x, t) f(x, t, k);
id = tic();
x = lsode(fk, x0, t);
[nt, nx] = size(x);

keyboard

N = 32 + 48 + 32;
DN = (32 + 48) * 2;
tsample = 1;
nsamples = nt - 1;
nsimus = 1;
samples = cell(N, nsamples / tsample * nsimus);
dsamples = cell(1, nsimus);

% plot(t,x);
% h = legend ("x1","x2","x3","x4");
% legend (h, "location", "northeastoutside");
% set (h, "fontsize", 20);
#keyboard
cpt = 1;

for s = 1:nsimus
    s
    x0 = x0; %[10*rand();10*rand();5*rand();5*rand()];
    x = lsode(fk, x0, t);
    dsamples{s} = cell(32 + 48, 10);

    for i = 1:tsample:10
        # SAMPLES FOR BN
        xd = discretize(x(i, :), var) +1; % /!\ discretize numerote à partir de zero /!\
        xdtp1 = discretize(x(i + 1, :), var) +1;

        for j = 1:32
            samples(j, cpt) = xd(j);
            samples(32 + 48 + j, cpt) = xdtp1(j);

            dsamples{s}(j, i) = xd(j);
        endfor

        cpt++;
    endfor

endfor

max_iter = 10;
% tic()
% bn2 = learn_params_em(engine, samples, max_iter);
% toc()

ncases = 2;
cases = cell(1, ncases);
T = 10;

for i = 1:ncases
    ev = sample_dbn(dbn, T);
    cases{i} = cell(32 + 48, T);
    cases{i}(onodes, :) = ev(onodes, :);
end

tic()
dbn2 = learn_params_dbn_em(dengine, dsamples, 'max_iter', max_iter);
toc()

% # for static Bnet:  engine = jtree_inf_engine(bnet);
% #engine = smoother_engine(hmm_2TBN_inf_engine(dbn));
% engine = jtree_inf_engine(bn);
% #engine = smoother_engine(jtree_2TBN_inf_engine(dbn));
% #ev = sample_dbn(dbn,2);
% evidence = cell(1,32+48+32);
% [engine, loglik] = enter_evidence(engine, evidence);
% marg = marginal_nodes(engine, 8);

% #evidence = cell(7,2);

% # APPRENTISSAGE DE PARAMETRES
% N = 32+48+32;
% nsamples = 10;
% samples = cell(N, nsamples);
% for i=1:nsamples
%   samples(:,i) = sample_bnet(bn);
% end

% % Make a tabula rasa
% %bnet2 = mk_bnet(dag_egf,[mk_nodesize(var) mk_nodesize(par) mk_nodesize(var)], 'names', {'x1','x2','x3','x4','k1','k2','k3','x1p','x2p','x3p','x4p'});
% bnet2 = mk_static_bayesian(var,par,dependencyvar,dependencypar, varname, parname, []);
% seed = 0;
% rand('state', seed);
% bnet3 = learn_params(bnet2, samples);

% CPT3 = cell(1,N);
% for i=1:N
%   s=struct(bnet3.CPD{i});  % violate object privacy
%   CPT3{i}=s.CPT;
% end
% dispcpt(CPT3{4})

% # APPRENTISSAGE PARAMETRES AVEC DONNEES INCOMPLETES

% samples2 = samples;
% hide = rand(N, nsamples) > 0.5;
% [I,J]=find(hide);
% for k=1:length(I)
%   samples2{I(k), J(k)} = [];
% end

% engine2 = jtree_inf_engine(bnet2);
% max_iter = 2;
% [bnet4, LLtrace] = learn_params_em(engine2, samples2, max_iter);

% CPT4 = cell(1,N);
% for i=1:N
%   s=struct(bnet4.CPD{i});  % violate object privacy
%   CPT4{i}=s.CPT;
% end
% dispcpt(CPT4{4})

% keyboard
