%EGF sim
warning("off");
addpath("../engine");
pkg load statistics;

NB_samples = 10; #nombre de trajectoires pour générer les CPT
%bucket_size = 5;

global var = csvread("var.csv");
var(1, :) = [];
var(:, 1) = [];
var(:, end) = [];

for i = 1:length(var)
    var(i, 4) = (var(i, 3) - var(i, 2)) / var(i, 4);
endfor

par = csvread("par.csv");
par(1, :) = [];
par(:, 1) = [];
par(:, end) = [];

for i = 1:length(par)
    par(i, 4) = (par(i, 3) - par(i, 2)) / par(i, 4);
endfor

function xdot = f (x, t, k)
    xdot(1)=-k(1) * x(1) * x(3) + k(2) * x(4);
    xdot(2)=-k(3) * x(2) * x(5) + k(4) * x(6);
    xdot(3)=-k(1) * x(1) * x(3) + k(2) * x(4);
    xdot(4) = k(1) * x(1) * x(3) - k(2) * x(4);
    xdot(5)=-k(3) * x(2) * x(5) + k(4) * x(6);
    xdot(6) = k(3) * x(2) * x(5) - k(4) * x(6);
    xdot(7) = k(9) * x(10) * x(8) / (x(8) + k(10)) - k(5) * x(4) * x(7) / (x(7) + k(6)) - k(7) * x(6) * x(7) / (x(7) + k(8));
    xdot(8)=-k(9) * x(10) * x(8) / (x(8) + k(10)) + k(5) * x(4) * x(7) / (x(7) + k(6)) + k(7) * x(6) * x(7) / (x(7) + k(8));
    xdot(9)=-k(27) * x(21) * x(9) / (x(9) + k(28));
    xdot(10) = k(27) * x(21) * x(9) / (x(9) + k(28));
    xdot(11)=-k(11) * x(8) * x(11) / (x(11) + k(12)) + k(13) * x(13) * x(12) / (x(12) + k(14));
    xdot(12) = k(11) * x(8) * x(11) / (x(11) + k(12)) - k(13) * x(13) * x(12) / (x(12) + k(14));
    xdot(13) = 0;
    xdot(14)=-k(15) * x(12) * x(14) / (x(14) + k(16)) + k(45) * x(32) * x(15) / (x(15) + k(46)) + k(35) * x(25) * x(15) / (x(15) + k(36));
    xdot(15) = k(15) * x(12) * x(14) / (x(14) + k(16)) - k(45) * x(32) * x(15) / (x(15) + k(46)) - k(35) * x(25) * x(15) / (x(15) + k(36));
    xdot(16)=-k(43) * x(29) * x(16) / (x(16) + k(44)) + k(47) * x(32) * x(17) / (x(17) + k(20)); %k(48) ???
    xdot(17) = k(43) * x(29) * x(16) / (x(16) + k(44)) - k(47) * x(32) * x(17) / (x(17) + k(20));
    xdot(18)=-k(17) * x(15) * x(18) / (x(18) + k(18)) - k(19) * x(17) * x(18) / (x(18) + k(48)) + k(21) * x(31) * x(19) / (x(19) + k(22));
    xdot(19) = k(17) * x(15) * x(18) / (x(18) + k(18)) + k(19) * x(17) * x(18) / (x(18) + k(48)) - k(21) * x(31) * x(19) / (x(19) + k(22));
    xdot(20)=-k(23) * x(19) * x(20) / (x(20) + k(24)) + k(25) * x(31) * x(21) / (x(21) + k(26));
    xdot(21) = k(23) * x(19) * x(20) / (x(20) + k(24)) - k(25) * x(31) * x(21) / (x(21) + k(26));
    xdot(22)=-k(29) * x(4) * x(22) / (x(22) + k(30)) - k(31) * x(12) * x(22) / (x(22) + k(32));
    xdot(23) = k(29) * x(4) * x(22) / (x(22) + k(30)) + k(31) * x(12) * x(22) / (x(22) + k(32));
    xdot(24)=-k(33) * x(23) * x(24) / (x(24) + k(34));
    xdot(25) = k(33) * x(23) * x(24) / (x(24) + k(34));
    xdot(26)=-k(37) * x(6) * x(26) / (x(26) + k(38));
    xdot(27) = k(37) * x(6) * x(26) / (x(26) + k(38));
    xdot(28)=-k(39) * x(27) * x(28) / (x(28) + k(40)) + k(41) * x(30) * x(29) / (x(29) + k(42));
    xdot(29) = k(39) * x(27) * x(28) / (x(28) + k(40)) - k(41) * x(30) * x(29) / (x(29) + k(42));
    xdot(30) = 0;
    xdot(31) = 0;
    xdot(32) = 0;
endfunction

varname = {"x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", "x12", "x13", "x14", "x15", "x16", "x17", "x18", "x19", "x20", "x21", "x22", "x23", "x24", "x25", "x26", "x27", "x28", "x29", "x30", "x31", "x32"};

parname = {"k1", "k2", "k3", "k4", "k5", "k6", "k7", "k8", "k9", "k10", "k11", "k12", "k13", "k14", "k15", "k16", "k17", "k18", "k19", "k20", "k21", "k22", "k23", "k24", "k25", "k26", "k27", "k28", "k29", "k30", "k31", "k32", "k33", "k34", "k35", "k36", "k37", "k38", "k39", "k40", "k41", "k42", "k43", "k44", "k45", "k46", "k47", "k48"};

nb_var = 32;
nb_par = 48;

dependencypar = zeros(nb_var, nb_par);
dependencypar(1, [1 2]) = 1;
dependencypar(2, [3 4]) = 1;
dependencypar(3, [1 2]) = 1;
dependencypar(4, [1 2]) = 1;
dependencypar(5, [3 4]) = 1;
dependencypar(6, [3 4]) = 1;
dependencypar(7, [5 6 7 8 9 10]) = 1;
dependencypar(8, [5 6 7 8 9 10]) = 1;
dependencypar(9, [27 28]) = 1;
dependencypar(10, [27 28]) = 1;
dependencypar(11, [11 12 13 14]) = 1;
dependencypar(12, [11 12 13 14]) = 1;
#dependencypar(13,[])= 1; #pas de parametre
dependencypar(14, [15 16 35 36 45 46]) = 1;
dependencypar(15, [15 16 35 36 45 46]) = 1;
dependencypar(16, [20 43 44 47]) = 1;
dependencypar(17, [20 43 44 47]) = 1;
dependencypar(18, [17 18 19 21 22 48]) = 1;
dependencypar(19, [17 18 19 21 22 48]) = 1;
dependencypar(20, [23 24 25 26]) = 1;
dependencypar(21, [23 24 25 26]) = 1;
dependencypar(22, [29 30 31 32]) = 1;
dependencypar(23, [29 30 31 32]) = 1;
dependencypar(24, [33 34]) = 1;
dependencypar(25, [33 34]) = 1;
dependencypar(26, [37 38]) = 1;
dependencypar(27, [37 38]) = 1;
dependencypar(28, [39 40 41 42]) = 1;
dependencypar(29, [39 40 41 42]) = 1;
#dependencypar(30,[])= 1;
#dependencypar(31,[])= 1;
#dependencypar(32,[])= 1;

dependencyvar = zeros(nb_var, nb_var);
dependencyvar(1, [1 3 4]) = 1;
dependencyvar(2, [2 5 6]) = 1;
dependencyvar(3, [1 3 4]) = 1;
dependencyvar(4, [1 3 4]) = 1;
dependencyvar(5, [2 5 6]) = 1;
dependencyvar(6, [2 5 6]) = 1;
dependencyvar(7, [4 6 7 8 10]) = 1;
dependencyvar(8, [4 6 7 8 10]) = 1;
dependencyvar(9, [9 21]) = 1;
dependencyvar(10, [9 10 21]) = 1; #ajout 10
dependencyvar(11, [11 12 13]) = 1;
dependencyvar(12, [11 12 13]) = 1;
dependencyvar(13, [13]) = 1;
dependencyvar(14, [12 14 15 25 32]) = 1;
dependencyvar(15, [12 14 15 25 32]) = 1;
dependencyvar(16, [16 17 29 32]) = 1;
dependencyvar(17, [16 17 29 32]) = 1;
dependencyvar(18, [15 17 18 19 31]) = 1;
dependencyvar(19, [15 17 18 19 31]) = 1;
dependencyvar(20, [19 20 21 31]) = 1;
dependencyvar(21, [19 20 21 31]) = 1;
dependencyvar(22, [4 12 22]) = 1;
dependencyvar(23, [4 12 22 23]) = 1; #ajout 23
dependencyvar(24, [23 24]) = 1;
dependencyvar(25, [23 24 25]) = 1; #ajout 25
dependencyvar(26, [6 26]) = 1;
dependencyvar(27, [6 26 27]) = 1; #ajout 27
dependencyvar(28, [27 28 29 30]) = 1;
dependencyvar(29, [27 28 29 30]) = 1;
dependencyvar(30, [30]) = 1;
dependencyvar(31, [31]) = 1;
dependencyvar(32, [32]) = 1;

% t = linspace(0,10,101);

addpath("../engine");

%Compute sim %%%%%%

% global timeode=0;
% global timemapid=0;
% id = tic();
% bncpd = gen_bncpd(var,par,dependencyvar, dependencypar,t,NB_samples,true);
% toc(id)
% timeode
% timemapid

% save("./CPDs/bayesian_cpd_50.mat", "bncpd");

%build bnet %%%%%
addpath("../bnt");
addpath(genpathKPM("../bnt"));
load("./CPDs/bayesian_cpd_1000000_b5");

onodes = [11 12 14 15 16 17 18 19 20 21 24 25];

bn = mk_static_bayesian(var, par, dependencyvar, dependencypar, varname, parname, bncpd);
dbn = mk_dynamic_bayesian(var, par, onodes, dependencyvar, dependencypar, varname, parname, bncpd);
% ### INFERENCE
% keyboard

engine = jtree_inf_engine(bn);
dengine = smoother_engine(jtree_2TBN_inf_engine(dbn));
%dengine = smoother_engine(hmm_2TBN_inf_engine(dbn));

k = sample_init(par);
[nk, _] = discretize(k, par);
nk++;

t = linspace(0, 10, 101);

x0 = var(:, 1)'; %var(:,1);%%
fk = @(x, t) f(x, t, k);
id = tic();
x = lsode(fk, x0, t);
[nt, nx] = size(x);

N = 32 + 48 + 32;
DN = (32 + 48) * 2;
B_size = 5;
B_size_par = 5;

oparams = [5 6 7 8 9 10 13 14 16 18 19 20 21 22 24 25 26 30 31 32 35 36 41 42 45 46 47 48];
uparams = [1 2 3 4 11 12 15 17 23 27 28 29 33 34 37 38 39 40 43 44];
vardim = 32;
pardim = 48;

ntime = 11;

% x0 = var(:,1)'; %var(:,1); %  %
x0 = sample_init(var);
M = bucket_middles(var, B_size_par);
timedata = [1, 11, 21, 31, 41, 51, 61, 71, 81, 91, 101];
evidence = cell(1, N);
x_init = discretize(x0, var);

for i = 1:vardim
    evidence{i} = x_init(i) + 1;
end

tab_res = [];

fk = @(x, t) f(x, t, k);
% id= tic();
lsode_options("integration method", "stiff");
% lsode_options("initial step size",0.001);

x = lsode(fk, x0, t);
[nt, nx] = size(x);

expdata = x(timedata, :);

k_random = sample_init(par);
par_init = discretize(k_random, par) + 1;
par_init(oparams) = 1;
% keyboard
n_x = pardim; % 'n_x' states
limits = repmat([1 5], n_x, 1); % Boundaries
limits(oparams, end) = 1;
obj = 0; % objective value (f(x_min) = obj)

nf = 1; % length of the output vector 'f(x,y)'
mu = 5; % parent population size
lambda = 5; % offspring population size
gen = 15; % number of generations
sel = '+'; % Selection scheme (Pag. 78 in (BACK))
rec_obj = 2; % Type of recombination to use on object
% variables (Pag. 74 in (BACK))
% See 'recombination.m'
rec_str = 4; % Type of recombination to use on strategy
% parameters (Pag. 74 in (BACK))
u = 0; % external excitation

% keyboard

% disp("SRES")
% tic()
% [min_x, min_f, off, EPS, idx] = evolution_strategy_discrete(mu, lambda, gen, sel, rec_obj, rec_str, u, obj, nf, n_x, limits, vardim, pardim, ntime, B_size_par, M, bn, expdata, timedata, evidence);
% toc()
% %
% disp ("Hooke jeeves std")
% tic()
% [err_res, res] = hooke_jeeves(par_init, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()
% disp ("Hooke jeeves min")
% tic()
% [err_res2, res2] = hooke_jeeves_min(par_init, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()
% disp ("Hooke jeeves mod")
% tic()
% [err_res3, res3] = hooke_jeeves_mod(par_init, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()

disp ("Inference search")
tic()
[err_res4, res4] = inference_search(var, par, t, bn, B_size, expdata, timedata, vardim, pardim, ntime, B_size_par, M, evidence, oparams, uparams);
toc()
% keyboard
disp ("Hooke jeeves std with inference init")
tic()
[err_res5, res5] = hooke_jeeves(res4, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
toc()

plot_fig

% disp ("Hooke jeeves min with inference init")
% tic()
% [err_res6, res6] = hooke_jeeves_min(res4, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()
% disp ("Hooke jeeves mod with inference init")
% tic()
% [err_res7, res7] = hooke_jeeves_mod(res4, evidence, vardim, pardim, ntime, B_size, M, bn, expdata, timedata, oparams, uparams);
% toc()

% keyboard

% b_size = 5;

% t = linspace(0, 100, 101);

% var_init = var(:,1);
% par_init = k;
% vec_middles = zeros(b_size,1);
% for i = 1:b_size
%     vec_middles(i) = 1/(2*b_size) + (i-1)*1/b_size;
% endfor

% par_best = par(:,2) + par(:,4).*vec_middles(res5);

% x = lsode(@(x, t) f(x, t, par_init), var_init, t);
% x_learned = lsode(@(x, t) f(x, t, par_best), var_init, t);

% subplot(2,2,1);
%     l1 = plot(t, x(:,2), "r");
%     hold on;
%     l2 = plot(t,x_learned(:,2), "--r");
%     xlabel("Time")
%     ylabel("NGF")
%     set(l1,"linewidth",2);
%     set(l2,"linewidth",2);
%     set(gca, "linewidth", 2, "fontsize", 12)

% subplot(2,2,2);
%     plot(t, x(:,5), "g");
%     hold on
%     plot(t,x_learned(:,5), "--g");
%     xlabel("Time")
%     ylabel("freeNGFReceptor")
%     set(l1,"linewidth",2);
%     set(l2,"linewidth",2);
%     set(gca, "linewidth", 2, "fontsize", 12)

% subplot(2,2,3);
%     plot(t, x(:,6), "b");
%     hold on
%     plot(t,x_learned(:,6), "--b");
%     xlabel("Time")
%     ylabel("boundNGFReceptor")
%     set(l1,"linewidth",2);
%     set(l2,"linewidth",2);
%     set(gca, "linewidth", 2, "fontsize", 12)

% subplot(2,2,4);
%     plot(t, x(:,9), "m");
%     hold on
%     plot(t,x_learned(:,9), "--m");
%     xlabel("Time")
%     ylabel("P90RskInactive")
%     set(l1,"linewidth",2);
%     set(l2,"linewidth",2);
%     set(gca, "linewidth", 2, "fontsize", 12)

keyboard
